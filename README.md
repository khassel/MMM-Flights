# MMM-Flights

This is a module for the [MagicMirror²-Project](https://github.com/MichMich/MagicMirror/).

It displays current flights in a defined area with a map containing the plane positions.

## Example

![](data/flights_osm.png)

You can use other maps, here a mapbox example:

![](data/flights_mapbox.png)

## Installation

Assuming `~/MagicMirror` is the directory where you installed MagicMirror².

### Clone and install

```bash
cd ~/MagicMirror/modules
git clone https://gitlab.com/khassel/MMM-Flights.git
cd MMM-Flights
npm install --omit=dev
```

### Update your config.js file

Add a configuration block to the modules array in the `~/MagicMirror/config/config.js` file and define the area
which is defined by a bounding box of WGS84 coordinates (la=latitude, lo=longitude):

```js
var config = {
  modules: [
    {
      module: "MMM-Flights",
      position: "top_left",
      config: {
        laMin: 50.0,
        laMax: 50.21,
        loMin: 8.4,
        loMax: 8.8,
      },
    },
  ],
};
```

This is the minimal config setup, you find more params in the [`default` section of `MMM-Flights.js`](../-/blob/master/MMM-Flights.js#L11).

As default map [openstreetmap](https://www.openstreetmap.org/copyright) is used, other maps are possible, you find a list [here](https://wiki.openstreetmap.org/wiki/Tile_servers).

You also can use maps from [mapbox](https://www.mapbox.com/maps) and create own maps with [mapbox studio](https://studio.mapbox.com/).

With mapbox you have to provide
- `<username>` (e.g. `karsten13`)
- `<mapid>` (e.g. `cjp8vd7p807su2rqp8hc4dzoq`)
- `<accesstoken>`

and set the `mapUrl` in `config.js` to `https://api.mapbox.com/styles/v1/<username>/<mapid>/tiles/{z}/{x}/{y}?access_token=<accesstoken>`.

## Update to new versions

Assuming `~/MagicMirror` is the directory where you installed MagicMirror².

```bash
cd ~/MagicMirror/modules/MMM-Flights
git pull
npm update --omit=dev
```

## Following a flight

If you have a mouse configured or a touchscreen you can follow a flight by clicking on it. The flight will be centered in the map and has another color and will be the first item in the list (which is normally ordered alphabetically).

## Flight data providers

Per default this project uses the npm module [flightradar24-client](https://www.npmjs.com/package/flightradar24-client) which uses an inofficial flightradar24 api. Meanwhile other providers are possible (configuration see [`default` section of `MMM-Flights.js`](../-/blob/master/MMM-Flights.js#L11)):

Restrictions:

- [Flightradar24](https://www.flightradar24.com): If you fetch to much data requests are blocked with `connection refused` so don't use to big areas or a to short update interval.
- [ADSBexchange](https://www.adsbexchange.com/): You find the api description [here](https://rapidapi.com/adsbx/api/adsbx-flight-sim-traffic).
  - You need an api key and you have a hard limit of 5.760 requests per month (with the free account).
  - No flight nr is provided so the callsign is used instead.
- [OpenSky](https://opensky-network.org/): You find the api description [here](https://openskynetwork.github.io/opensky-api/rest.html).
  - Anonymous users have a hard limit of 100 requests per day.
  - With user and password (account is free) you have a hard limit of 1.000 requests per day.
  - No flight nr is provided so the callsign is used instead.
  - No airport data (from/to) is provided.
  - No plane type is provided.

## Known issues and limitations

- May incomplete or outdated data: The airline, airport and plane data is added from static textfiles in this repository. If something is missing let me know and I will add it.
