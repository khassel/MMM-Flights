/**
 * @file node_helper.js
 *
 * @author Karsten Hassel
 * @license MIT
 *
 * @see  https://gitlab.com/khassel/MMM-Flights
 */

const NodeHelper = require("node_helper");
const mmObj = require("magicmirror-helper/mm_helper");
const fs = require("fs");
const readline = require("readline");
const path = require("path");
const Log = require("logger");

module.exports = NodeHelper.create(
  Object.assign({}, mmObj, {
    airlines: [],
    airports: [],
    planes: [],
    providers: [],
    lastFlightData: null,
    mockFile: __dirname + "/data/data.json",

    start() {
      Log.log(`Starting module helper: ${this.name}`);

      const providerFolder = __dirname + "/provider/";
      fs.readdirSync(providerFolder).forEach((file) => {
        if (file.includes(".js"))
          this.providers.push(file.toLowerCase().replace(".js", ""));
      });

      this.airlines = JSON.parse(
        fs.readFileSync(path.join(__dirname, "data", "airlines.json"), "utf8")
      );

      this.airports = JSON.parse(
        fs
          .readFileSync(path.join(__dirname, "data", "airports.json"), "utf8")
          .replace(/ Airport/g, "")
          .replace(/ International/g, "")
      );

      readline
        .createInterface({
          input: fs.createReadStream(
            path.join(__dirname, "data", "planes.txt")
          ),
          crlfDelay: Infinity,
        })
        .on("line", (line) => {
          this.planes.push(line);
        });
    },

    socketNotificationReceived(notification, payload) {
      if (notification.includes("CONFIG_MMM_FLIGHTS_")) {
        this.addPayload(payload.instanceId, payload);
        this.getData(payload);
      }
    },

    /**
     * Returns the destination point from a given point, having travelled the given distance
     * on the given initial bearing.
     *
     * @param   {number} lat - initial latitude in decimal degrees (eg. 50.123)
     * @param   {number} lon - initial longitude in decimal degrees (e.g. -4.321)
     * @param   {number} distance - Distance travelled (metres).
     * @param   {number} bearing - Initial bearing (in degrees from north).
     * @returns {array} destination point as [latitude,longitude] (e.g. [50.123, -4.321])
     *
     * @example
     *     const p = destinationPoint(51.4778, -0.0015, 7794, 300.7); // 51.5135°N, 000.0983°W
     */
    destinationPoint(lat, lon, distance, bearing) {
      const toRadians = (v) => (v * Math.PI) / 180;
      const toDegrees = (v) => (v * 180) / Math.PI;
      const radius = 6371e3;
      const δ = Number(distance) / radius;
      const θ = toRadians(Number(bearing));
      const φ1 = toRadians(Number(lat));
      const λ1 = toRadians(Number(lon));
      const sinφ1 = Math.sin(φ1);
      const cosφ1 = Math.cos(φ1);
      const sinδ = Math.sin(δ);
      const cosδ = Math.cos(δ);
      const sinθ = Math.sin(θ);
      const cosθ = Math.cos(θ);
      const sinφ2 = sinφ1 * cosδ + cosφ1 * sinδ * cosθ;
      const φ2 = Math.asin(sinφ2);
      const y = sinθ * sinδ * cosφ1;
      const x = cosδ - sinφ1 * sinφ2;
      const λ2 = λ1 + Math.atan2(y, x);
      return [toDegrees(φ2), ((toDegrees(λ2) + 540) % 360) - 180];
    },

    setFlightData(flight, airlines, planes) {
      const setAirline = (flight) => {
        const airline = airlines.find((obj) => {
          return obj.icao_code === flight.callsign.substring(0, 3);
        });
        flight.airline = airline ? airline.name : "";
      };

      const setPlaneType = (flight) => {
        const pName = planes.find(
          (row) => row.substring(0, flight.model.length) === flight.model
        );
        flight.planeType = pName
          ? pName.substring(flight.model.length + 1)
          : "";
      };

      flight.airline = "";
      flight.planeType = "";
      if (flight.callsign && flight.callsign !== "") {
        setAirline(flight);
      }
      if (flight.model && flight.model !== "") {
        setPlaneType(flight);
      }
    },

    sortFlights(flights, currentFlight) {
      const compare = (a, b) => {
        let akey = a.flight;
        if (akey == null) akey = a.radar;
        let bkey = b.flight;
        if (bkey == null) bkey = b.radar;

        if (akey < bkey) {
          return -1;
        }
        if (akey > bkey) {
          return 1;
        }
        return 0;
      };
      flights.sort(compare);

      // move to first position
      if (currentFlight) {
        flights.sort((obj) => (obj.flight === currentFlight ? -1 : 0));
      }
    },

    async makeFlightArray(configPayload) {
      const flights = [];

      const prov = configPayload.provider.toLowerCase();
      if (this.providers.includes(prov)) {
        await require(`./provider/${prov}`).getData(
          configPayload,
          flights,
          this.airports,
          this.airlines,
          this.planes,
          this.setFlightData,
          this.fetchUrl
        );
      } else {
        Log.error(
          `${this.name}: Unknown provider  + ${configPayload.provider}`
        );
      }

      return flights;
    },

    async getData(configPayload) {
      try {
        if (configPayload.useMockData) {
          //file exists
          if (fs.existsSync(this.mockFile)) {
            Log.warn(`${this.name}: using mock data !!!`);
            fs.readFile(this.mockFile, "utf-8", (err, content) => {
              if (err) {
                Log.error(`${this.name}: error reading mock data ${err}`);
              } else {
                const data = JSON.parse(content.toString());
                this.sortFlights(data, configPayload.currentFlight);
                const sendData = {
                  flights: data,
                  edges: [],
                };
                this.sendSocketNotification(
                  "UPDATE_MMM_FLIGHTS_" + configPayload.instanceId,
                  sendData
                );
              }
            });
          } else {
            Log.error(
              `${this.name}: mock data file "${this.mockFile}" not exists.`
            );
          }
        } else {
          let edges = [];
          if (this.lastFlightData && configPayload.currentFlight) {
            const currFlight = this.lastFlightData.find((obj) => {
              return obj.flight === configPayload.currentFlight;
            });
            if (currFlight) {
              // speed in mph, updateInterval in ms
              const distance =
                (currFlight.speed * 1.609344 * configPayload.updateInterval) /
                3600;
              const newLatLon = this.destinationPoint(
                currFlight.latitude,
                currFlight.longitude,
                distance,
                currFlight.bearing
              );
              const w = Math.abs(configPayload.loMax - configPayload.loMin) / 2;
              const h = Math.abs(configPayload.laMax - configPayload.laMin) / 2;
              configPayload.loMin = newLatLon[1] - w;
              configPayload.loMax = newLatLon[1] + w;
              configPayload.laMin = newLatLon[0] - h;
              configPayload.laMax = newLatLon[0] + h;
              edges = [
                configPayload.loMin,
                configPayload.laMin,
                configPayload.loMax,
                configPayload.laMax,
              ];
            }
          }

          const flights = await this.makeFlightArray(configPayload);
          this.sortFlights(flights, configPayload.currentFlight);

          // fs.writeFile(this.mockFile, JSON.stringify(flights), (err) => {
          //   if (err) {
          //     throw err;
          //   }
          //   Log.log(`${this.name}: JSON data is saved.`);
          // });
          // Log.dir(flights);

          const sendData = {
            flights: flights,
            edges: edges,
          };

          this.lastFlightData = flights;
          this.sendSocketNotification(
            "UPDATE_MMM_FLIGHTS_" + configPayload.instanceId,
            sendData
          );
        }
      } catch (e) {
        Log.error(`${this.name}: unknown error getting data ${e}`);
      }
    },
  })
);
