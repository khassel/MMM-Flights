/**
 * @file flightradar24.js
 *
 * @author Karsten Hassel
 * @license MIT
 *
 * @see  https://gitlab.com/khassel/MMM-Flights
 */

getData = async (
  configPayload,
  flights,
  airports,
  airlines,
  planes,
  setFlightData,
  fetchUrl
) => {
  const getAirportNameByIata = (iata) => {
    const airport = airports.find((obj) => {
      return obj.iata === iata;
    });
    if (airport) {
      return airport.name;
    } else {
      return "";
    }
  };

  const frModule = await import("flightradar24-client/lib/radar.js");
  const radar = frModule.fetchFromRadar;

  const data = await radar(
    configPayload.laMax,
    configPayload.loMin,
    configPayload.laMin,
    configPayload.loMax
  );

  for (let i in data) {
    if (configPayload.showGrounded || !data[i].isOnGround) {
      const fo = new Object();
      fo.flight = data[i].flight;
      if (!fo.flight || fo.flight === "") fo.flight = data[i].radar;
      fo.registration = data[i].registration;
      fo.callsign = data[i].callsign;
      fo.radar = data[i].radar;
      fo.fromCode = data[i].origin;
      fo.fromName = getAirportNameByIata(fo.fromCode);
      fo.toCode = data[i].destination;
      fo.toName = getAirportNameByIata(fo.toCode);
      fo.latitude = data[i].latitude;
      fo.longitude = data[i].longitude;
      fo.altitude = data[i].altitude;
      fo.bearing = data[i].bearing;
      fo.speed = data[i].speed;
      fo.rateOfClimb = data[i].rateOfClimb;
      fo.isOnGround = data[i].isOnGround;
      fo.model = data[i].model;

      setFlightData(fo, airlines, planes);
      flights.push(fo);
    }
  }
};

module.exports = { getData };
