/**
 * @file opensky.js
 *
 * @author Karsten Hassel
 * @license MIT
 *
 * @see  https://gitlab.com/khassel/MMM-Flights
 */

getData = async (
  configPayload,
  flights,
  airports,
  airlines,
  planes,
  setFlightData,
  fetchUrl
) => {
  const getOpenskyData = async (config) => {
    const { providerApiKey, providerUrl, laMin, loMin, laMax, loMax } = config;
    const url = `${providerUrl}?lamin=${laMin}&lomin=${loMin}&lamax=${laMax}&lomax=${loMax}`;
    let options = {};
    if (providerApiKey && providerApiKey !== "") {
      options = {
        headers: {
          Authorization: `Basic ${Buffer.from(providerApiKey).toString(
            "base64"
          )}`,
        },
      };
    }
    const { states } = await fetchUrl(url, options, "json");
    return states;
  };

  const data = await getOpenskyData(configPayload);
  for (let i in data) {
    // api: https://openskynetwork.github.io/opensky-api/rest.html
    if (configPayload.showGrounded || !data[i][8]) {
      const fo = new Object();
      fo.flight = data[i][1]; // callsign, FlightNr. not available
      fo.transponder = data[i][0];
      fo.registration = ""; // not available
      fo.callsign = data[i][1];
      fo.radar = ""; // not available
      fo.fromCode = ""; // not available
      fo.fromName = ""; // not available
      fo.toCode = ""; // not available
      fo.toName = ""; // not available

      fo.latitude = data[i][6];
      fo.longitude = data[i][5];
      fo.altitude = data[i][13] / 0.3048;
      fo.bearing = data[i][10];
      fo.speed = data[i][9] * 2.23694; // provided in m/s
      fo.isOnGround = data[i][8];
      fo.rateOfClimb = data[i][11];
      fo.planeType = ""; // not available, nur grobe Klassifizierung siehe model
      fo.country = data[i][2];
      setFlightData(fo, airlines, planes);
      fo.model = ""; // data[i][17] is mentioned in the docs but not delivered
      flights.push(fo);
    }
  }
};

module.exports = { getData };
