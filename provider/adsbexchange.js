/**
 * @file adsbexchange.js
 *
 * @author Karsten Hassel
 * @license MIT
 *
 * @see  https://gitlab.com/khassel/MMM-Flights
 */

getData = async (
  configPayload,
  flights,
  airports,
  airlines,
  planes,
  setFlightData,
  fetchUrl
) => {
  const getADBSData = async (baseUrl, apiKey, lat, lon) => {
    const url = `${baseUrl}/lat/${lat}/lon/${lon}/dist/25/`;
    const options = {
      headers: {
        "X-RapidAPI-Key": apiKey,
        "X-RapidAPI-Host": "adsbx-flight-sim-traffic.p.rapidapi.com",
      },
    };
    const { ac } = await fetchUrl(url, options, "json");
    return ac;
  };

  const isFlightInArea = (flight, config) => {
    return (
      flight.longitude >= config.loMin &&
      flight.longitude <= config.loMax &&
      flight.latitude >= config.laMin &&
      flight.latitude <= config.laMax
    );
  };

  const lat =
    configPayload.laMin + (configPayload.laMax - configPayload.laMin) / 2;
  const lon =
    configPayload.loMin + (configPayload.loMax - configPayload.loMin) / 2;
  const data = await getADBSData(
    configPayload.providerUrl,
    configPayload.providerApiKey,
    lat,
    lon
  );
  for (let i in data) {
    const onGround =
      data[i].gnd === "1" || (data[i].spd == 0 && fo.altitude == 0);
    if (configPayload.showGrounded || !onGround) {
      const fo = new Object();
      fo.flight = data[i].call; // FlightNr. not available
      fo.registration = data[i].reg;
      fo.callsign = data[i].call;
      fo.radar = ""; // not available
      const fr = data[i].from;
      if (fr) {
        fo.fromCode = fr.substring(0, 3);
        fo.fromName = fr.substring(4, 50);
      }
      const to = data[i].to;
      if (to) {
        fo.toCode = to.substring(0, 3);
        fo.toName = to.substring(4, 50);
      }

      fo.latitude = data[i].lat;
      fo.longitude = data[i].lon;
      fo.altitude = data[i].alt; // todo: alt or galt?
      if (fo.altitude < 0) fo.altitude = 0;
      fo.bearing = data[i].trak;
      fo.speed = data[i].spd;
      fo.isOnGround = onGround;
      fo.rateOfClimb = 0;
      if (!fo.isOnGround && Math.abs(data[i].vsi) > 100) {
        fo.rateOfClimb = data[i].vsi;
      }
      fo.model = data[i].type;

      setFlightData(fo, airlines, planes);
      if (isFlightInArea(fo, configPayload)) {
        flights.push(fo);
      }
    }
  }
};

module.exports = { getData };
