/**
 * @file MMM-Flights.js
 *
 * @author Karsten Hassel
 * @license MIT
 *
 * @see  https://gitlab.com/khassel/MMM-Flights
 */

Module.register("MMM-Flights", {
  defaults: {
    provider: "flightradar24",
    instanceId: "FR", // needs to be set to different values if you run more than 1 instance of this module
    // // example for ADSBexchange:
    // provider: "ADSBexchange",
    // providerApiKey: "your-api-key",
    // providerUrl: "https://adsbx-flight-sim-traffic.p.rapidapi.com/api/aircraft/json",
    // // example for opensky:
    // provider: "opensky",
    // providerApiKey: "user:password", //optional, without limited requests
    // providerUrl: "https://opensky-network.org/api/states/all",
    unknown: "?", // string used if no data available
    title: "Flights", // only visible if module header not defined
    logoTitle: "fas fa-plane",
    logoUp: "fas fa-plane-departure",
    logoDown: "fas fa-plane-arrival",
    logoTo: "fas fa-arrow-right",
    // area defined by a bounding box of WGS84 coordinates (la=latitude, lo=longitude):
    laMin: 49.85,
    laMax: 50.25,
    loMin: 7.8,
    loMax: 9.3,
    showGrounded: false, // show grounded airplanes
    showMap: "always", // "always", "never", "ifTraffic"
    showTable: true,
    // you can add own filters which flights you want to see by defining this in config.js with your filter
    filterFlights(flight) {
      return true; // default no filter, returns all flights
      // example for returning only flights for a specific airline
      // return flight.airline && flight.airline.includes("TUI Fly")
    },
    noFlightsMessage: "Currently no flights in chosen area ...", // message shown if no data available
    mapUrl: "https://{a-d}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png", // you can bring your own map
    mapAttributions:
      '© <a href="https://www.openstreetmap.org/copyright" style="color: #000000">OpenStreetMap contributors</a>',
    mapHeight: "300px",
    mapFixedArea: true, // if set, the defined area is shown, if not the area is calculated from the plane locations
    mapPosition: "top", // "top" or "bottom"
    // some params to define styles in map:
    mapFont: "40% sans-serif",
    mapPadding: [1, 5, 0, 7],
    mapFontColor: "white",
    mapBackgroundColor: "black",
    mapCurrentFlightColor: "#9c294f",
    mapBackgroundOpacity: 0.5,
    mapPlanePngSrc: "modules/MMM-Flights/data/plane.png",
    mapPlanePngScale: 0.07,
    mapPlaneTextOffset: 4,
    mapPlaneTextDirection: "withPlane", // "withPlane" or "horizontal"
    mapGetPlaneText(flight) {
      if (flight.flight) return flight.flight;
      return flight.radar;
    },
    useMockData: false, // if set, test data is used
    updateInterval: 2 * 60 * 1000, // every 2 minutes
    units: "metric", // metric uses m and km/h, imperial uses ft and mph
  },

  edge: "◉",
  wrapper: null,
  mapDiv: null,
  listDiv: null,
  mh: null,
  firstUpdate: true,

  getScripts() {
    return [
      "https://cdn.jsdelivr.net/npm/ol@10.4.0/dist/ol.js",
      `./modules/${this.name}/node_modules/magicmirror-helper/mapHandler.js`,
    ];
  },

  getStyles() {
    return ["font-awesome.css", "MMM-Flights.css"];
  },

  start() {
    Log.info(`Starting module: ${this.name}_${this.config.instanceId}`);
    this.sendSocketNotification(
      "CONFIG_MMM_FLIGHTS_" + this.config.instanceId,
      this.config
    );
  },

  addMap() {
    Log.debug(`${this.name}_${this.config.instanceId} add map`);

    const olStyle = new ol.style.Style({
      image: new ol.style.Icon({
        src: this.config.mapPlanePngSrc,
        scale: 0,
      }),
      text: new ol.style.Text({
        font: this.config.mapFont,
        fill: new ol.style.Fill({
          color: this.config.mapFontColor,
        }),
        padding: this.config.mapPadding,
        backgroundFill: new ol.style.Fill({
          // will be overwritten anyway
          color: "black",
        }),
      }),
    });

    const styleFunc = (feature) => {
      const txtStyle = olStyle.getText();
      const setOpacity = (colorString, opacity) => {
        let colorArray = [0, 0, 0, 0];
        if (colorString) {
          const fill = new ol.style.Fill({
            color: colorString,
          });
          colorArray = ol.color.asArray(fill.getColor()).slice();
        }
        colorArray[3] = opacity;
        txtStyle.getBackgroundFill().setColor(colorArray);
      };
      const txt = feature.get("text");
      const deg = (feature.get("deg") * Math.PI) / 180;
      const imgStyle = olStyle.getImage();
      if (txt === this.edge) {
        // scale image down to nothing
        imgStyle.setScale(0);
        // set background to transparant
        setOpacity(null, 0);
      } else {
        if (this.config.currentFlight === txt) {
          setOpacity(
            this.config.mapCurrentFlightColor,
            this.config.mapBackgroundOpacity
          );
        } else {
          setOpacity(
            this.config.mapBackgroundColor,
            this.config.mapBackgroundOpacity
          );
        }
        imgStyle.setScale(this.config.mapPlanePngScale);
        imgStyle.setRotation(deg);
        txtStyle.setOffsetY(this.config.mapPlaneTextOffset);
        if (this.config.mapPlaneTextDirection === "withPlane")
          txtStyle.setRotation(deg);
      }
      txtStyle.setText(txt);
      return olStyle;
    };

    this.mh = new mapHandler(
      this.config,
      `${this.name}_${this.config.instanceId}`,
      this.mapDiv,
      styleFunc
    );

    // use click because singleclick is delayed by 250 ms
    this.mh.map.on("click", (evt) => {
      const pixel = this.mh.map.getEventPixel(evt.originalEvent);
      const features = [];
      this.mh.map.forEachFeatureAtPixel(pixel, (feature) => {
        features.push(feature);
      });
      if (features.length > 0) {
        this.config.lastFlight = this.config.currentFlight;
        this.config.currentFlight = features[0].get("text");
        const coord = features[0].getGeometry().getCoordinates();
        this.mh.map.getView().setCenter(coord);
        if (this.config.lastFlight !== this.config.currentFlight) {
          Log.info(
            `${this.name}_${this.config.instanceId}: Following flight ${this.config.currentFlight}`
          );
          this.sendSocketNotification(
            "CONFIG_MMM_FLIGHTS_" + this.config.instanceId,
            this.config
          );
        }
      }
    });
  },

  getHeader() {
    return getModuleHeader(this.data, this.config);
  },

  getDom() {
    Log.debug(`${this.name}_${this.config.instanceId} create dom`);

    this.wrapper = document.createElement("div");
    this.wrapper.id = `wrapper_${this.name}_${this.config.instanceId}`;

    this.listDiv = document.createElement("div");
    this.listDiv.id = `table_${this.name}_${this.config.instanceId}`;
    this.listDiv.classList.add("align-left");
    this.listDiv.classList.add("small");
    this.listDiv.innerHTML = "Loading ...";

    const newLine = document.createElement("div");
    newLine.style.height = "5px";

    if (this.config.showTable && this.config.mapPosition !== "top") {
      this.wrapper.appendChild(this.listDiv);
      this.wrapper.appendChild(newLine);
    }

    if (this.config.showMap !== "never") {
      this.mapDiv = document.createElement("div");
      this.mapDiv.id = `map_${this.name}_${this.config.instanceId}`;
      this.mapDiv.classList.add("map");
      this.mapDiv.style.height = `${this.config.mapHeight}`;
      this.wrapper.appendChild(this.mapDiv);
      this.addMap();
    }

    if (this.config.showTable && this.config.mapPosition === "top") {
      this.wrapper.appendChild(newLine);
      this.wrapper.appendChild(this.listDiv);
    }

    return this.wrapper;
  },

  updateList(flights) {
    Log.debug(`${this.name}_${this.config.instanceId} update list`);

    // default text when no flights:
    this.listDiv.innerHTML = this.config.noFlightsMessage;

    if (flights.length > 0) {
      this.listDiv.innerHTML = "";
      let cell;
      const addCell = (parent, html, classList = []) => {
        cell = document.createElement("td");
        parent.appendChild(cell);
        cell.innerHTML = html;
        classList.forEach((cl) => {
          cell.classList.add(cl);
        });
      };

      const tbl = document.createElement("table");
      tbl.classList.add("table");
      this.listDiv.appendChild(tbl);
      flights.forEach((f) => {
        const row1 = document.createElement("tr");
        row1.classList.add("regular");
        tbl.appendChild(row1);
        addCell(row1, `${f.flight}&nbsp;&nbsp;${f.airline}`);
        if (f.planeType) {
          addCell(row1, `${f.planeType}`);
        } else if (f.country) {
          addCell(row1, `${f.country}`);
          if (f.transponder) {
            addCell(row1, `${f.transponder}`, ["small"]);
          } else {
            addCell(row1, "", ["small"]);
          }
        } else {
          addCell(row1, `${this.config.unknown}`);
        }
        if (f.rateOfClimb < 0) {
          addCell(row1, `<i class="${this.config.logoDown}"></i>`, ["right"]);
        } else if (f.rateOfClimb > 0) {
          addCell(row1, `<i class="${this.config.logoUp}"></i>`, ["right"]);
        } else {
          addCell(row1, "", ["right"]);
        }
        if (this.config.units === "metric") {
          addCell(row1, (f.altitude * 0.3048).toFixed() + " m", ["right"]);
          addCell(row1, (f.speed * 1.609344).toFixed() + " km/h", ["right"]);
        } else {
          addCell(row1, f.altitude.toFixed() + " ft", ["right"]);
          addCell(row1, f.speed.toFixed() + " mph", ["right"]);
        }
        const row2 = document.createElement("tr");
        row2.classList.add("regular");
        row2.classList.add("dimmed");
        tbl.appendChild(row2);
        if (f.fromCode === "" && f.toCode === "") {
          addCell(row2, "", ["small"]);
        } else {
          let from = f.fromName;
          if (f.fromCode) {
            from = `${from} (${f.fromCode})`;
          } else {
            from = `${from} ${this.config.unknown}`;
          }
          let to = f.toName;
          if (f.toCode) {
            to = `${to} (${f.toCode})`;
          } else {
            to = `${to} ${this.config.unknown}`;
          }
          addCell(
            row2,
            `${from}&nbsp;<i class="${this.config.logoTo}"></i>&nbsp;${to}`,
            ["small"]
          );
        }
        if (f.registration) {
          addCell(row2, `${f.registration}`, ["small"]);
        } else {
          addCell(row2, "", ["small"]);
        }
      });
    }
  },

  updateMap(sendData) {
    Log.debug(`${this.name}_${this.config.instanceId} update map`);

    const flights = sendData.flights;

    this.mh.clearPoints();

    const centerArea = () => {
      const geom = ol.geom.Polygon.fromExtent(this.mh.sourcePoints.getExtent());
      geom.scale(1.1);
      this.mh.map.getView().fit(geom, { maxZoom: 12 });
    };

    const addAreaPoints = (reset) => {
      if (this.config.mapFixedArea) {
        let loMin = this.config.loMin;
        let laMin = this.config.laMin;
        let loMax = this.config.loMax;
        let laMax = this.config.laMax;

        if (!reset) {
          loMin = sendData.edges[0] ? sendData.edges[0] : loMin;
          laMin = sendData.edges[1] ? sendData.edges[1] : laMin;
          loMax = sendData.edges[2] ? sendData.edges[2] : loMax;
          laMax = sendData.edges[3] ? sendData.edges[3] : laMax;
        }

        this.mh.addMapPoint(loMin, laMin, this.edge, 0);
        this.mh.addMapPoint(loMin, laMax, this.edge, 0);
        this.mh.addMapPoint(loMax, laMin, this.edge, 0);
        this.mh.addMapPoint(loMax, laMax, this.edge, 0);
      }
    };

    flights.forEach((flight) => {
      this.mh.addMapPoint(
        flight.longitude,
        flight.latitude,
        this.config.mapGetPlaneText(flight),
        flight.bearing
      );
    });

    if (this.firstUpdate) {
      this.firstUpdate = false;
      addAreaPoints(true);
      centerArea();
    } else if (this.config.currentFlight && this.config.currentFlight !== "") {
      const flight = flights.find((obj) => {
        return obj.flight === this.config.currentFlight;
      });

      if (flight) {
        addAreaPoints(false);
        const coord = ol.proj.fromLonLat([flight.longitude, flight.latitude]);
        this.mh.map.getView().setCenter(coord);
      } else {
        this.config.currentFlight = null;
        this.sendSocketNotification(
          "CONFIG_MMM_FLIGHTS_" + this.config.instanceId,
          this.config
        );
      }
    } else {
      addAreaPoints(true);
      centerArea();
    }
  },

  socketNotificationReceived(notification, payload) {
    const delay = (time) => {
      return new Promise((resolve) => setTimeout(resolve, time));
    };

    if (notification === "UPDATE_MMM_FLIGHTS_" + this.config.instanceId) {
      payload.flights = payload.flights.filter(this.config.filterFlights);
      if (this.config.showTable) this.updateList(payload.flights);
      if (this.config.showMap !== "never") {
        this.mapDiv.style.height = `${this.config.mapHeight}`;
        this.updateMap(payload);
        if (
          this.config.showMap === "ifTraffic" &&
          payload.flights.length === 0
        ) {
          this.mapDiv.style.height = "0px";
        } else if (this.config.showMap === "ifTraffic") {
          // ugly hack for repaint with correct zoom:
          delay(100).then(() => this.updateMap(payload));
        }
      }
    }
  },
});
