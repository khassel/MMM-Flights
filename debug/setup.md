## Start Containers

```bash
cd /mnt/c/data/repo/k13/MMM-Flights

docker run --rm -it -u root -v $(pwd)/debug:/opt/magic_mirror/config -v $(pwd):/opt/magic_mirror/modules/MMM-Flights -p 8080:8080 karsten13/magicmirror:develop bash

# only for npm i
docker run --rm -it -v $(pwd):/workspace node:alpine sh
```

## todo's

- lastFlightData in node_helper must be per ID (?)
- how to check for ol updates (version in script url)